#* ************************************************************************** *#
#*                                                                            *#
#*                                                        :::      ::::::::   *#
#*   Makefile                                           :+:      :+:    :+:   *#
#*                                                    +:+ +:+         +:+     *#
#*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        *#
#*                                                +#+#+#+#+#+   +#+           *#
#*   Created: 2016/09/01 15:20:46 by aaverty           #+#    #+#             *#
#*   Updated: 2016/10/15 11:30:29 by aaverty          ###   ########.fr       *#
#*                                                                            *#
#* ************************************************************************** *#

.PHONY: all clean fclean re test test_bonus

NAME = ContentSquare
CC = gcc
SRC_PATH = ./src/
SRC_NAME =  main.c utils.c move.c
CFLAGS = -g -Wall -Wextra -Werror
LIB = -L./libft/ -lft
LIB_NAME = libft/libft.a
LIB_INC_PATH = libft/inc/
INC_PATH = ./inc/
OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

all: $(NAME)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@echo "\033[36m$(NAME):\033[0m [\033[35mCompilation:\033[0m\033[32m $@\033[0m]"
	@$(CC) -o $@ -c $< $(CFLAGS) -I$(INC_PATH) -I$(LIB_INC_PATH)

$(NAME): $(LIB_NAME) $(OBJ)
	@$(CC) $(CFLAGS) -o $@ $(LIB) $^
	@echo "[\033[36m------------------------------------------\033[0m]"
	@echo "[\033[36m------------ $(NAME) - OK ----------\033[0m]"
	@echo "[\033[36m------------------------------------------\033[0m]"

$(LIB_NAME):
	@make -C libft/

clean:
	@rm -rf $(OBJ_PATH)
	@make clean -C libft/

fclean: clean
	@rm -f $(NAME)
	@make fclean -C libft/

re: fclean all

test:
	./ContentSquare
	@echo
	./ContentSquare test/test.txt
	@echo
	./ContentSquare test/test_hard.txt

test_bonus:
	@echo
	./ContentSquare -v test/test.txt
	@echo
	./ContentSquare -v test/test_hard.txt

