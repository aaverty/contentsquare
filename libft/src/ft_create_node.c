/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/12 18:01:23 by aaverty           #+#    #+#             */
/*   Updated: 2015/12/30 19:20:27 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_node			*ft_create_node(void)
{
	t_node	*new;
	int		i;

	i = 0;
	new = malloc(sizeof(*new));
	if (new != NULL)
	{
		while (i < 4)
		{
			new->pos[i].x = 0;
			new->pos[i].y = 0;
			i++;
		}
		new->next = NULL;
	}
	return (new);
}
