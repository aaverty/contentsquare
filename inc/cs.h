/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cs.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/14 16:46:01 by aaverty           #+#    #+#             */
/*   Updated: 2016/10/15 11:00:25 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CS_H
# define CS_H

#include "libft.h"

typedef struct	s_position
{
	int			x;
	int			y;
}				t_position;

typedef struct	s_tondeuse
{
	char		orientation;
	char		*deplacement;
	t_position	position;
}				t_tondeuse;

typedef struct	s_grille
{
	int			width;
	int			heigth;
}				t_grille;

typedef struct	s_app
{
	t_grille	grille;
	t_list		**lst_tondeuses;
	int			v;
}				t_app;


int				is_orientation(char *orientation);
int				verif_pos(t_grille grille, t_tondeuse tondeuse);
int				verif_move(char *move);
void			ft_error(char *error);
void			move(t_app *ap, int fd);
void			get_grille(t_app *ap, int fd);
void			get_tondeuses(t_app *ap, int fd);

#endif
