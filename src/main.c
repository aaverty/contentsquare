/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/15 09:35:31 by aaverty           #+#    #+#             */
/*   Updated: 2016/10/15 11:18:40 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cs.h"
#include <fcntl.h>

static void			print(t_app *app)
{
	t_list		*lst;
	t_list		*tmp;
	t_tondeuse	*tondeuse;

	lst = *app->lst_tondeuses;
	while (lst)
	{
		tondeuse = (t_tondeuse*)lst->content;
		ft_printf("%d %d %c\n", tondeuse->position.x, tondeuse->position.y, tondeuse->orientation);
		tmp = lst;
		lst = lst->next;
		free(tondeuse->deplacement);
		free(tmp->content);
		free(tmp);
	}
}

void				get_grille(t_app *app, int fd)
{
	char	*line;
	char	**tab;
	int		ret;

	tab = NULL;
	if ((ret = ft_get_next_line(fd, &line)) == -1)
		ft_error("Error: syntax file is invalid !");
	else
	{
		tab = ft_strsplit(line, ' ');
		if (tab[2] == NULL && ft_str_is_numeric(tab[0]) && ft_str_is_numeric(tab[1]))
		{
			app->grille.width = ft_atoi(tab[0]);
			app->grille.heigth = ft_atoi(tab[1]);
		}
		else
			ft_error("Error: syntax grid is invalid !");
	}
	free(line);
	free(tab[0]);
	free(tab[1]);
	free(tab);
}

static t_tondeuse	get_position_and_orientation(t_app *app, char **tab)
{
	t_tondeuse tondeuse;

	tondeuse.position.x = 0;
	tondeuse.position.y = 0;
	tondeuse.orientation = 0;
	tondeuse.deplacement = NULL;
	if (tab[0] && tab[1] && tab[2] && tab[3] == NULL
							&& ft_str_is_numeric(tab[0])
							&& ft_str_is_numeric(tab[1])
							&& is_orientation(tab[2]))
	{
		tondeuse.position.x = ft_atoi(tab[0]);
		tondeuse.position.y = ft_atoi(tab[1]);
		tondeuse.orientation = tab[2][0];
		if (!verif_pos(app->grille, tondeuse))
			ft_error("Error: position is out of the grid !");
	}
	else
		ft_error("Error: position or orientation syntax is invalid !");
	free(tab[0]);
	free(tab[1]);
	free(tab[2]);
	free(tab);
	tab = NULL;
	return (tondeuse);
}

void				get_tondeuses(t_app *app, int fd)
{
	int			ret;
	char		**tab;
	char		*line;
	t_tondeuse	tondeuse;
	t_list		*tmp;

	while ((ret = ft_get_next_line(fd, &line)) != -1 && ft_strcmp(line, ""))
	{
		tab = ft_strsplit(line, ' ');
		free(line);
		tondeuse = get_position_and_orientation(app, tab);
		if (app == NULL)
			ft_error("Error: syntax file is invalid !");
		if ((ret = ft_get_next_line(fd, &line)) == -1 || !ft_strcmp(line, ""))
			ft_error("Error: syntax file is invalid !");
		if (verif_move(line))
			tondeuse.deplacement = line;
		else
			ft_error("Error: move syntax is invalid !");
		if (app->lst_tondeuses == NULL)
		{
			tmp = ft_lstnew(&tondeuse, sizeof(tondeuse));
			app->lst_tondeuses = &tmp;
		}
		else
			ft_lst_push_back(app->lst_tondeuses, ft_lstnew(&tondeuse, sizeof(tondeuse)));
	}
	free(line);
}

int					main(int argc, char **argv)
{
	t_app	app;
	int		fd;

	fd = 0;
	app.lst_tondeuses = NULL;
	if (argc > 1)
	{
		if (argc == 2)
			fd = open(argv[1], O_RDONLY);
		else if (argc == 3)
		{
			app.v = 1;
			fd = open(argv[2], O_RDONLY);
		}
		if (fd == -1)
			ft_error("Error: file name is invalid !");
		else
		{
			move(&app, fd);
			(!app.v) ? print(&app) : 0;
		}
	}
	else
		ft_printf("usage: ContentSquare [-v] [file]\n");
	return (0);
}
