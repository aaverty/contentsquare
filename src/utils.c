/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/15 09:36:08 by aaverty           #+#    #+#             */
/*   Updated: 2016/10/15 09:49:45 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cs.h"

int		is_orientation(char *orientation)
{
	if (ft_strcmp(orientation, "N") == 0
			|| ft_strcmp(orientation, "n") == 0
			|| ft_strcmp(orientation, "E") == 0
			|| ft_strcmp(orientation, "e") == 0
			|| ft_strcmp(orientation, "W") == 0
			|| ft_strcmp(orientation, "w") == 0
			|| ft_strcmp(orientation, "S") == 0
			|| ft_strcmp(orientation, "s") == 0)
		return (1);
	else
		return (0);
}

int		verif_move(char *move)
{
	int	i;
	int	count;

	i = 0;
	count = 0;
	while (move[i])
	{
		if (move[i] == 'A' || move[i] == 'a' ||
				move[i] == 'G' || move[i] == 'g' ||
				move[i] == 'D' || move[i] == 'd')
			count++;
		i++;
	}
	if (count == i)
		return (1);
	else
		return (0);
}

int		verif_pos(t_grille grille, t_tondeuse tondeuse)
{
	if (tondeuse.position.x >= 0 && tondeuse.position.y >= 0
			&& tondeuse.position.x <= grille.width
			&& tondeuse.position.y <= grille.heigth)
		return (1);
	else
		return (0);
}

void	ft_error(char *error)
{
	ft_printf("@{red}%s{eoc}\n", 2, error);
	exit(1);
}
