/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/15 09:35:42 by aaverty           #+#    #+#             */
/*   Updated: 2016/10/15 10:57:41 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cs.h"

static void		rotate_left(t_app *app, t_tondeuse *tondeuse)
{
	(app->v) ? ft_printf("{cyan}Rotation à gauche{eoc}\n{green}Orientation "
			"actuelle : %c{eoc}\n", ft_toupper(tondeuse->orientation)) : 0;
	if (tondeuse->orientation == 'N' || tondeuse->orientation == 'n')
		tondeuse->orientation = 'W';
	else if (tondeuse->orientation == 'E' || tondeuse->orientation == 'E')
		tondeuse->orientation = 'N';
	else if (tondeuse->orientation == 'W' || tondeuse->orientation == 'w')
		tondeuse->orientation = 'S';
	else if (tondeuse->orientation == 'S' || tondeuse->orientation == 's')
		tondeuse->orientation = 'E';
	(app->v) ? ft_printf("{green}Orientation après rotation: %c{eoc}\n\n",
			ft_toupper(tondeuse->orientation)) : 0;
}

static void		rotate_rigth(t_app *app, t_tondeuse *tondeuse)
{
	(app->v) ? ft_printf("{magenta}Rotation à droite{eoc}\n{green}Orientation "
			"actuelle : %c{eoc}\n", ft_toupper(tondeuse->orientation)) : 0;
	if (tondeuse->orientation == 'N' || tondeuse->orientation == 'n')
		tondeuse->orientation = 'E';
	else if (tondeuse->orientation == 'E' || tondeuse->orientation == 'E')
		tondeuse->orientation = 'S';
	else if (tondeuse->orientation == 'W' || tondeuse->orientation == 'w')
		tondeuse->orientation = 'N';
	else if (tondeuse->orientation == 'S' || tondeuse->orientation == 's')
		tondeuse->orientation = 'W';
	(app->v) ? ft_printf("{green}Orientation après rotation: %c{eoc}\n\n",
			ft_toupper(tondeuse->orientation)) : 0;
}

static void	moving_forward(t_app *app, t_tondeuse *tondeuse)
{
	int		x;
	int		y;

	x = tondeuse->position.x;
	y = tondeuse->position.y;
	(app->v) ? ft_printf("{blue}Déplacement{eoc}\n{green}Postion x de départ: "
			"%d | Postion y de départ: %d | Orientation actuelle : %c{eoc}\n",
			x, y, ft_toupper(tondeuse->orientation)) : 0;
	if (tondeuse->orientation == 'N' || tondeuse->orientation == 'n')
		y++;
	else if (tondeuse->orientation == 'E' || tondeuse->orientation == 'E')
		x++;
	else if (tondeuse->orientation == 'W' || tondeuse->orientation == 'w')
		x--;
	else if (tondeuse->orientation == 'S' || tondeuse->orientation == 's')
		y--;
		(x >= 0 && x <= app->grille.width) ? tondeuse->position.x = x : 0;
		(y >= 0 && y <= app->grille.heigth) ? tondeuse->position.y = y : 0;
	(app->v) ? ft_printf("{green}Postion x d'arrivé: %d | Postion y d'arrivé: "
			"%d | Orientation après déplacement: %c{eoc}\n\n", x, y,
			ft_toupper(tondeuse->orientation)) : 0;
}

void		move(t_app *app, int fd)
{
	char		*deplacement;
	int			i;
	int			j;
	t_list		*lst;
	t_tondeuse	*tondeuse;

	get_grille(app, fd);
	get_tondeuses(app, fd);
	lst = *app->lst_tondeuses;
	j = 1;
	while (lst)
	{
		i = 0;
		tondeuse = (t_tondeuse*)lst->content;
		deplacement = tondeuse->deplacement;
		(app->v) ? ft_printf("<cyan>{blue}Tondeuse %d:{eoc}<eoc>\n", j) : 0;
		while (deplacement && deplacement[i])
		{
			if (deplacement[i] == 'G' || deplacement[i] == 'g')
				rotate_left(app, (t_tondeuse*)lst->content);
			else if (deplacement[i] == 'D' || deplacement[i] == 'd')
				rotate_rigth(app, (t_tondeuse*)lst->content);
			else if (deplacement[i] == 'A' || deplacement[i] == 'a')
				moving_forward(app, (t_tondeuse*)lst->content);
			i++;
		}
		(app->v) ? ft_printf("<cyan>{blue}Tondeuse %d:{eoc}<eoc>\n%d %d %c\n"
				"{red}----------------------{eoc}\n", j, tondeuse->position.x,
				tondeuse->position.y, tondeuse->orientation) : 0;
		(app->v && lst->next != NULL) ? write(1, "\n\n", 2) : 0;
		lst = lst->next;
		j++;
	}
}
